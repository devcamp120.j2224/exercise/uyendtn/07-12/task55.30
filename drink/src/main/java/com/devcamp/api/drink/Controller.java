package com.devcamp.api.drink;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class Controller {
    @GetMapping("/drinks")
    public ArrayList<CDrink> getDrink() {
        
        ArrayList<CDrink> drinkMenu = new ArrayList<CDrink>();
        CDrink tratac = new CDrink();
        CDrink coca = new CDrink("COCA");
        CDrink pepsi = new CDrink("PEPSI","Pepsi");
        CDrink lavie = new CDrink("LAVIE","Lavie",5000d);
        CDrink fanta = new CDrink("FANTA","Fanta",15000d,"");
        
        drinkMenu.add(tratac);
        drinkMenu.add(coca);
        drinkMenu.add(pepsi);
        drinkMenu.add(lavie);
        drinkMenu.add(fanta);
        
        return drinkMenu;
    } 
}
